package jag.parser.comb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * An immutable sequential collection
 * @param <T> the type of the items in the collection
 */
public class Seq<T> {
    private static final Seq NIL = new Seq<>(null, null, 0, 0);
    private final int size;
    private final int hashCode;
    private final T head;
    private final Seq<T> tail;

    private Seq(T head, Seq<T> tail, int size, int hashCode) {
        this.head = head;
        this.tail = tail;
        this.size = size;
        this.hashCode = hashCode;
    }

    /** @return if this Seq is not empty, then the first item wrapped in an Optional. otherwise, Optional#empty */
    public Optional<T> getHead() { return Optional.ofNullable(head); }

    /** @return if this Seq contains more than 1 element, then the elements after the first wrapped in an Optional.
     * otherwise, Optional#empty */
    public Optional<Seq<T>> getTail() { return Optional.ofNullable(tail); }

    /** @return the number of items in this Seq */
    public int getSize() { return size; }

    /** @return true iff this Seq contains no items */
    public boolean isEmpty() { return this == NIL; }

    /** @return a copy of this but with fn applied to each value */
    public <A> Seq<A> map(final Function<T,A> fn) {
        return foldR((x,xs) -> cons(fn.apply(x),xs), empty());
    }

    /** @return a Seq consisting of the concatenation of the result of applying fn to each value in this */
    public <A> Seq<A> flatMap(final Function<T,Seq<A>> fn) {
        return foldL(empty(), (xs,x) -> xs.concat(fn.apply(x)));
    }

    /** @return the result of falding this from the left with fn */
    public <A> A foldL(final A z, final BiFunction<A,T,A> fn) {
        return isEmpty() ? z : tail.foldL(fn.apply(z, head), fn);
    }

    /** @return the result of folding this from the right with fn */
    public <A> A foldR(final BiFunction<T,A,A> fn, A z) {
        return isEmpty() ? z : fn.apply(head, tail.foldR(fn,z));
    }

    /** @return the elements of this Seq followed by the elements of s. order remains unchanged */
    public Seq<T> concat(final Seq<T> s) {
        return reverse().foldL(s, (xs,x) -> cons(x,xs));
    }

    /** @return the elements of this Seq in reverse order */
    public Seq<T> reverse() {
        return foldL(empty(), (xs,x) -> cons(x,xs));
    }

    /** @return the elements of this Seq in a LinkedList. O(n) */
    public List<T> toLinkedList() {
        return into(LinkedList::new);
    }

    /** @return the elements of this Seq in an ArrayList.  O(n) */
    public List<T> toArrayList() {
        return into(ArrayList::new);
    }

    /** @return the String formed by the characters in chars */
    public static String toString(final Seq<Character> chars) {
        final StringBuilder sb = new StringBuilder();
        chars.forEach(sb::append);
        return sb.toString();
    }

    private List<T> into(final Supplier<List<T>> coll) {
        final List<T> l = coll.get();
        Seq<T> next = this;
        while (!next.isEmpty()) {
            l.add(next.head);
            next = next.tail;
        }
        return l;
    }

    /**
     * @param <A> the type of the elements that the Seq may contain
     * @return the Seq containing no elements
     */
    @SuppressWarnings("unchecked")
    public static <A> Seq<A> empty() { return NIL; }

    /**
     * @param head the first item of the new Seq
     * @param tail the other items of the new Seq
     * @param <A> the type of the items that may be in the Seq
     * @return a new Seq consisting of head followed by tail
     */
    public static <A> Seq<A> cons(final A head, final Seq<A> tail) {
        if (head == null) { throw new IllegalArgumentException("head is null"); }
        if (tail == null) { throw new IllegalArgumentException("tail is null"); }
        return new Seq<>(head, tail, 1 + tail.size, head.hashCode() * (1 + tail.size) + tail.hashCode);
    }

    /** @return a Seq containing the elements of as with order preserved.  O(n) */
    public static <A> Seq<A> from(final List<A> as) {
        Seq<A> r = empty();
        for (int i = as.size()-1; i >= 0; i--) {
            r = cons(as.get(i), r);
        }
        return r;
    }

    /** @return a Seq containing the elements of as with order preserved.  O(n) */
    @SafeVarargs
    @SuppressWarnings("unchecked")
    public static <A> Seq<A> from(final A... as) {
        Seq<A> r = empty();
        for (int i = as.length-1; i >= 0; i--) {
            r = cons(as[i], r);
        }
        return r;
    }

    /** @return a Seq containing the Characters of str with order preserved.  O(n) */
    public static Seq<Character> from(final String str) {
        Seq<Character> r = empty();
        for (int i = str.length()-1; i >= 0; i--) {
            r = cons(str.charAt(i), r);
        }
        return r;
    }

    /** apply the given procedure to each element of the Seq in order */
    public void forEach(final Consumer<T> f) {
        if (!isEmpty()) {
            f.accept(head);
            tail.forEach(f);
        }
    }

    @Override
    public boolean equals(final Object other) {
        return other != null && other instanceof Seq && eq(this, (Seq)other);
    }

    private static boolean eq(Seq a, Seq b) {
        return a == b || (a.size == b.size && a.head != null && a.head.equals(b.head) && eq(a.tail, b.tail));
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public String toString() { return toArrayList().toString(); }
}
