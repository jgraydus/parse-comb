package jag.parser.comb;

/** The type with a single inhabitant (not counting null) */
public final class Unit {
    public static final Unit unit = new Unit();
    private Unit() {}
}
