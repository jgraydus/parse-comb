package jag.parser.comb;

import java.util.List;
import java.util.Optional;

/**
 * Source represents a source of the tokens to be parsed
 * @param <T> the type of the token
 */
public interface Source<T> {
    /** @return if the input is not exhausted, then a Pair consisting of the next item and a new Source representing
     * the rest of the input wrapped in Optional#of. if the input is exhaused, then Optional#empty */
    Optional<Pair<T,Source<T>>> next();

    /** @return the position in the input of the next item from this Source */
    int getPosition();

    /**
     * @return a Source of type A from the items in as with position as the index of the first item
     */
    static <A> Source<A> from(final Seq<A> as, int position) {
        return new Source<A>() {
            @Override
            public Optional<Pair<A,Source<A>>> next() {
                return as.getHead().map(a -> Pair.of(a, from(as.getTail().orElse(Seq.empty()),position+1)));
            }
            @Override
            public int getPosition() {
                return position;
            }
        };
    }

    /** @return a Source from the items in as */
    static <A> Source<A> from(final Seq<A> as) {
        return from(as, 0);
    }

    /** @return a Source from the characters in str */
    static Source<Character> from(final String str) {
        return from(Seq.from(str));
    }

    /** @return a Source from the items in as */
    static <A> Source<A> from(final List<A> as) {
        return from(Seq.from(as));
    }
}
