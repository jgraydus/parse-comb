package jag.parser.comb;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static jag.parser.comb.Result.failure;
import static jag.parser.comb.Result.success;
import static jag.parser.comb.Seq.cons;
import static jag.parser.comb.Unit.unit;

/**
 * @param <S> the type of the token of the source
 * @param <R> the type of the result of the parser
 */
public interface Parser<S,R> {
    Result<S,R> apply(final Source<S> source);

    /** @return a Parser which is the same as this except that on success fn will be applied to the result */
    default <T> Parser<S,T> map(final Function<R,T> fn) {
        return src -> apply(src).map(fn);
    }

    /** @return a Parser produced by fn from the result of this */
    default <T> Parser<S,T> flatMap(final Function<R,Parser<S,T>> fn) {
        return src -> apply(src).onSuccess(r -> fn.apply(r.get()).apply(r.getSource()));
    }

    /** @return a Parser which runs this followed by p. if both succeed, then the value produced by this is ignored
     * and the volue produced by p is kept */
    default <T> Parser<S,T> ignoreThen(final Parser<S,T> p) {
        return flatMap(ignored -> p);
    }

    /** @return a Parser which runs this followed by p. if both succeed, then the value produced by p is ignored
     * and the value produced by this is kept */
    default Parser<S,R> thenIgnore(final Parser<S,?> p) {
        return flatMap(x -> p.map(ignored -> x));
    }

    /** @return a Parser whose result is a Seq of the results of each Parser in ps (preserving order) if and only if
    * each Parser in ps succeeds. Otherwise, failure. */
    static <A,B> Parser<A,Seq<B>> sequence(final Seq<Parser<A,B>> ps) {
        return src -> ps.getHead()
                .flatMap(h -> ps.getTail().map(t -> h.flatMap(x -> sequence(t).map(xs -> cons(x,xs))).apply(src)))
                .orElse(success(Seq.empty(), src));
    }

    /** @return the result of this parser if it succeeds. otherwise, the result of p */
    default Parser<S,R> or(final Parser<S,R> p) {
        return src -> apply(src).onFailure(x -> p.apply(src));
    }

    /** @return a Parser that consumes no input and always succeeds, producing the given value */
    static <A,B> Parser<A,B> succeed(final B b) {
        return src -> success(b,src);
    }

    /** @return a Parser that consumes no input and always fails */
    static <A,B> Parser<A,B> fail() {
        return src -> failure("fail", src);
    }

    /** @return a Parser that repeats p until p fails, collecting the results into a Seq. if p fails the first time,
     * this Parser will succeed with a result of an empty Seq. <br>
     * <br>
     * Warning: you will most likely get a stack overflow in the case that p either always succeeds or succeeds
     * without consuming input */
    static <A,B> Parser<A,Seq<B>> atLeast0(final Parser<A,B> p) {
        return atLeast1(p).or(succeed(Seq.empty()));
    }

    /** @return a Parser that repeats p until p fails, collecting the results into a Seq if p succeeded at least
     * once. <br>
     * <br>
     * Warning: you will most likely get a stack overflow in the case that p either always succeeds or succeeds
     * without consuming input */
    static <A,B> Parser<A,Seq<B>> atLeast1(final Parser<A,B> p) {
        return p.flatMap(x -> atLeast0(p).map(xs -> cons(x,xs)));
    }

    /** @return a Parser which tries each of the Parsers in ps in order. as soon as one succeeds, that is the result.
     * if none of the Parsers in ps succeed, the result is failure */
    static <A,B> Parser<A,B> oneOf(final Seq<Parser<A,B>> ps) {
        return ps.getHead().flatMap(h -> ps.getTail().map(t -> h.or(oneOf(t)))).orElse(fail());
    }

    /** @return a Parser which tries each of the Parsers in ps in order. as soon as one succeeds, that is the result.
     * if none of the Parsers in ps succeed, the result is failure */
    @SafeVarargs
    static <A,B> Parser<A,B> oneOf(final Parser<A,B>... ps) {
        return oneOf(Seq.from(ps));
    }

    /** @return a Parser that accepts the next token from the input source if the given predicate results in true
     * when tests on that token */
    static <A> Parser<A,A> satisfies(final Predicate<A> pred) {
        return src -> src.next()
                .flatMap(n -> pred.test(n.getL()) ? Optional.of(success(n.getL(), n.getR())) : Optional.empty())
                .orElse(failure("satisfies failed", src));
    }

    /** @return a Parser that recognizes only the given character */
    static Parser<Character,Character> character(final char c) {
        return satisfies(x -> x.equals(c));
    }

    /** @return a Parser that recognizes any single letter */
    static Parser<Character,Character> letter() {
        return satisfies(Character::isLetter);
    }

    /** @return a Parser that recognizes any single digit */
    static Parser<Character,Character> digit() {
        return satisfies(Character::isDigit);
    }

    /** @return a Parser that recognizes only the given string */
    static Parser<Character,String> string(final String str) {
        return sequence(Seq.from(str).map(Parser::character)).map(x -> str);
    }

    /** @return a Parser that succeeds only when given an empty source. the name comes from "end of file" */
    static <A> Parser<A,Unit> eof() {
        return src -> src.next().isPresent() ? failure("expected end of input", src) : success(unit, src);
    }

    /** @return a Parser that first runs left, then runs this, then runs right. If all three are successful, the
     * resulting value is the value that this produced. The values that left and right produce are ignored */
    default Parser<S,R> surroundedBy(final Parser<S,?> left, final Parser<S,?> right) {
        return left.ignoreThen(this).thenIgnore(right);
    }

    /** @return a Parser that recognizes whatever this recognizes separated by whatever sep recognizes. the results
     * of this are collected into a Seq. the results of sep are discarded */
    default Parser<S,Seq<R>> separatedBy(final Parser<S,?> sep) {
        return flatMap(x -> atLeast0(sep.ignoreThen(this)).map(xs -> cons(x,xs))).or(succeed(Seq.empty()));
    }
}