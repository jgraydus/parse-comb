package jag.parser.comb;

import java.util.function.Function;

/**
 * @param <S> the type of the token of the source
 * @param <R> the type of the result of the parser
 */
public interface Result<S,R> {

    /** @return the resulting value
     *  @throws RuntimeException if the result is a failure
     */
    R get();

    /** @return if this is a Failure, then a Failure. if this is a Success, then a new Success with fn applied to the
     * value */
    <T> Result<S,T> map(final Function<R,T> fn);

    /** @return if this is a Failure, then a Failure. if this is a Success, applies fn to this */
    <T> Result<S,T> onSuccess(final Function<Success<S,R>,Result<S,T>> fn);

    /** @return if this is a Failure, then the result of applying fn to this. otherwise, this. */
    Result<S,R> onFailure(final Function<Failure<S,R>,Result<S,R>> fn);


    /** @return a Failure */
    static <A,B> Result<A,B> failure(final String message, final Source<A> s) {
        return new Failure<>(message, s);
    }

    /** @return a Success */
    static <A,B> Result<A,B> success(final B b, final Source<A> a) {
        return new Success<>(b, a);
    }

    /** @return true iff r is a Success */
    static boolean isSuccess(final Result<?,?> r) {
        return r instanceof Success;
    }


    /** represents the result of a failed Parser */
    final class Failure<S,R> implements Result<S,R> {
        private final String message;
        private final Source<S> s;

        private Failure(final String message, final Source<S> s) {
            this.message = message;
            this.s = s;
        }

        @Override
        public R get() { throw new RuntimeException(message + ", position=" + s.getPosition()); }

        Source<S> getSource() { return s; }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Result<S,T> map(final Function<R,T> fn) {
            return (Result<S,T>)this;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Result<S,T> onSuccess(final Function<Success<S,R>,Result<S,T>> fn) {
            return (Result<S,T>)this;
        }

        @Override
        public Result<S,R> onFailure(final Function<Failure<S,R>,Result<S,R>> fn) {
            return fn.apply(this);
        }
    }

    /** represents the result of a successful parser and contains the resulting value */
    final class Success<S,R> implements Result<S,R> {
        private final R t;
        private final Source<S> s;

        private Success(final R t, final Source<S> s) { this.t = t; this.s = s; }

        @Override
        public R get() { return t; }

        Source<S> getSource() { return s; }

        @Override
        public <T> Result<S,T> map(final Function<R,T> fn) {
            return success(fn.apply(t), s);
        }

        @Override
        public <T> Result<S,T> onSuccess(final Function<Success<S,R>,Result<S,T>> fn) {
            return fn.apply(this);
        }

        @Override
        public Result<S,R> onFailure(final Function<Failure<S,R>,Result<S,R>> fn) {
            return this;
        }
    }
}
