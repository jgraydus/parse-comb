package jag.parser.comb;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A generic container to hold a pair of items
 * @param <L> the type of the 'left' item
 * @param <R> the type of the 'right' item
 */
public interface Pair<L,R> {
    L getL();
    R getR();

    /** @return a Pair with a as the 'left' item and b as the 'right' item */
    static <A,B> Pair<A,B> of(final A a, final B b) {
        return new Pair<A,B>() {
            @Override public A getL() { return a; }
            @Override public B getR() { return b; }
        };
    }

    /** @return a copy of this Pair with fn applied to the 'left' item */
    default <T> Pair<T,R> lmap(final Function<L,T> fn) {
        return Pair.of(fn.apply(getL()), getR());
    }

    /** @return a copy of this Pair with fn applied to the 'right' item */
    default <T> Pair<L,T> rmap(final Function<R,T> fn) {
        return Pair.of(getL(), fn.apply(getR()));
    }

    /** @return a copy of this Pair with lfn applied to the 'left' item and rfn applied to the 'right' item */
    default <S,T> Pair<S,T> bimap(final Function<L,S> lfn, final Function<R,T> rfn) {
        return Pair.of(lfn.apply(getL()), rfn.apply(getR()));
    }

    /** @return a copy of this pair but with the 'left' item in the 'right' item position and vice versa */
    default Pair<R,L> flip() {
        return of(getR(), getL());
    }

    /** @return the value produced by applying fn to the 'left' and 'right' items of this Pair */
    default <T> T withBoth(final BiFunction<L,R,T> fn) {
        return fn.apply(getL(), getR());
    }
}
