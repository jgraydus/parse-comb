package jag.parser.comb;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PairTests {

    @Test
    public void test01() {
        final Pair<Integer,String> p = Pair.of(1, "abc");
        assertEquals(1, (int)p.getL());
        assertEquals("abc", p.getR());
    }

    @Test
    public void lmap01() {
        final Pair<Integer,String> p = Pair.of(1, "abc");
        assertEquals(2, (int)p.lmap(x -> x+1).getL());
        assertEquals("abc", p.getR());
    }

    @Test
    public void rmap01() {
        final Pair<Integer,String> p = Pair.of(1, "abc");
        assertEquals(1, (int)p.getL());
        assertEquals("abcdef", p.rmap(x -> x + "def").getR());
    }

    @Test
    public void bimap01() {
        final Pair<Integer,String> p = Pair.of(1, "abc").bimap(x -> x+1, x -> x + "def");
        assertEquals(2, (int)p.getL());
        assertEquals("abcdef", p.getR());
    }

    @Test
    public void flip01() {
        final Pair<String,Integer> p = Pair.of(1, "abc").flip();
        assertEquals("abc", p.getL());
        assertEquals(1, (int)p.getR());
    }

    @Test
    public void withBoth() {
        final Pair<Integer,String> p = Pair.of(1, "abc");
        assertEquals("1abc", p.withBoth((left,right) -> left + right));
    }
}
