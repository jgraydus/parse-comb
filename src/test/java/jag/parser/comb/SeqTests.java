package jag.parser.comb;

import org.junit.Test;

import java.util.List;

import static jag.parser.comb.Seq.*;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class SeqTests {

    @Test
    public void map01() {
        final Seq<Integer> s1 = from(1,2,3,4);
        final List<Integer> actual = s1.map(x -> x+1).toLinkedList();
        final List<Integer> expected = asList(2,3,4,5);
        assertEquals(expected, actual);
    }

    @Test
    public void flatMap01() {
        final Seq<Integer> s1 = from(1,2,3,4);
        final List<Integer> actual = s1.flatMap(x -> from(x,x+1,x+2)).toLinkedList();
        final List<Integer> expected = asList(1,2,3,2,3,4,3,4,5,4,5,6);
        assertEquals(expected, actual);
    }

    @Test
    public void concat01() {
        final Seq<Integer> s1 = from(1,2,3);
        final Seq<Integer> s2 = from(4,5,6);
        final List<Integer> actual = s1.concat(s2).toArrayList();
        final List<Integer> expected = asList(1,2,3,4,5,6);
        assertEquals(expected, actual);
    }

    @Test
    public void foldL01() {
        final Seq<Integer> s1 = from(1,2,3,4);
        final int actual = s1.foldL(0, (x,y) -> x+y);
        final int expected = 10;
        assertEquals(expected, actual);
    }

    @Test
    public void foldR01() {
        final Seq<Integer> s1 = from(1,2,3,4);
        final int actual = s1.foldR((x,y) -> x+y, 0);
        final int expected = 10;
        assertEquals(expected, actual);
    }

    @Test
    public void reverse01() {
        final Seq<Integer> s1 = cons(1, cons(2, cons(3, cons(4, empty()))));
        final List<Integer> actual = s1.reverse().toArrayList();
        final List<Integer> expected = asList(4,3,2,1);
        assertEquals(expected, actual);
    }

    @Test
    public void toString01() {
        final Seq<Character> s = empty();
        assertEquals("", Seq.toString(s));
    }

    @Test
    public void toString02() {
        final Seq<Character> s = from('a', 'b', 'c', 'd');
        assertEquals("abcd", Seq.toString(s));
    }
}
