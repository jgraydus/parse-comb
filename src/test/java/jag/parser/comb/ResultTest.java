package jag.parser.comb;

import org.junit.Test;
import static jag.parser.comb.Result.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ResultTest {

    private final Source<Character> src = Source.from("");

    @Test
    public void isSuccess01() {
        final Result<Character,Character> r = success(null, null);
        assertTrue(isSuccess(r));
    }

    @Test
    public void isSuccess02() {
        final Result<Character,Character> r = failure("",src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void map01() {
        final Result<Character,Integer> r = success('a', src).map(x -> 1);
        assertTrue(isSuccess(r));
        assertEquals(1, (int)r.get());
    }

    @Test
    public void map02() {
        final Result<Character,Character> r = failure("", src);
        assertFalse(isSuccess(r.map(x -> 1)));
    }

    @Test
    public void onSuccess01() {
        final Result<Character,Character> r1 = success('a', src);
        final Result<Character,Integer> r2 = r1.onSuccess(s -> success(1, s.getSource()));
        assertTrue(isSuccess(r2));
        assertEquals(1, (int)r2.get());
    }

    @Test
    public void onSuccess02() {
        final Result<Character,Character> r1 = failure("", src);
        final Result<Character,Integer> r2 = r1.onSuccess(s -> success(1, s.getSource()));
        assertFalse(isSuccess(r2));
    }

    @Test
    public void onFailure01() {
        final Result<Character,Character> r1 = success('a', src);
        final Result<Character,Character> r2 = r1.onFailure(s -> success('b', s.getSource()));
        assertTrue(isSuccess(r2));
        assertEquals('a', (char)r2.get());
    }

    @Test
    public void onFailure02() {
        final Result<Character,Character> r1 = failure("",src);
        final Result<Character,Integer> r2 = r1.onSuccess(s -> success(1, s.getSource()));
        assertFalse(isSuccess(r2));
    }
}
