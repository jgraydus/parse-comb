package jag.parser.comb;

import org.junit.Test;

import static jag.parser.comb.Parser.*;
import static jag.parser.comb.Result.isSuccess;
import static jag.parser.comb.Seq.cons;
import static jag.parser.comb.Seq.empty;
import static jag.parser.comb.Source.from;
import static org.junit.Assert.*;

public class ParserTests {

    @Test
    public void satisfies01() {
        final Source<Character> src = from("adgnreb");
        final Result<Character,Character> r = Parser.<Character>satisfies(x -> x.equals('a')).apply(src);
        assertTrue(isSuccess(r));
        assertEquals('a', (char)r.get());
    }

    @Test
    public void satisfies02() {
        final Source<Character> src = from("basdf");
        final Result<Character,Character> r = Parser.<Character>satisfies(x -> x.equals('a')).apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void satisfies03() {
        final Source<Integer> src = from(Seq.from(1,2,3));
        final Result<Integer,Integer> r = Parser.<Integer>satisfies(x -> x.equals(1)).apply(src);
        assertTrue(isSuccess(r));
        assertEquals(1, (int)r.get());
    }

    @Test
    public void satisfies04() {
        final Source<Integer> src = from(Seq.from(2,3,4));
        final Result<Integer,Integer> r = Parser.<Integer>satisfies(x -> x.equals(1)).apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void character01() {
        final Source<Character> src = from("asdf");
        final Result<Character,Character> r = character('a').apply(src);
        assertTrue(isSuccess(r));
        assertEquals('a', (char)r.get());
    }

    @Test
    public void character02() {
        final Source<Character> src = from("asdf");
        final Result<Character,Character> r = character('b').apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void letter01() {
        final Source<Character> src = from("asdf");
        final Result<Character,Character> r = letter().apply(src);
        assertTrue(isSuccess(r));
        assertEquals('a', (char)r.get());
    }

    @Test
    public void letter02() {
        final Source<Character> src = from("1asdf");
        final Result<Character,Character> r = letter().apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void digit01() {
        final Source<Character> src = from("1asdf");
        final Result<Character,Character> r = digit().apply(src);
        assertTrue(isSuccess(r));
        assertEquals('1', (char)r.get());
    }

    @Test
    public void digit02() {
        final Source<Character> src = from("asdf");
        final Result<Character,Character> r = digit().apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void string01() {
        final Source<Character> src = from("blah blah blakjdsakljsfd");
        final Result<Character,String> r = string("blah").apply(src);
        assertTrue(isSuccess(r));
        assertEquals("blah", r.get());
    }

    @Test
    public void string02() {
        final Source<Character> src = from("blah blah blakjdsakljsfd");
        final Result<Character,String> r = string("asfasdf").apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void map01() {
        final Object obj = new Object();
        final Source<Character> src = from("asdf");
        final Result<Character,Object> r = letter().map(x -> obj).apply(src);
        assertTrue(isSuccess(r));
        assertEquals(obj, r.get());
    }

    @Test
    public void flatMap01() {
        final Source<Character> src = from("abcde");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,Seq<Character>> ab = a.flatMap(x -> b.map(y -> Seq.from(x,y)));
        final Result<Character,Seq<Character>> r = ab.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.from('a','b'), r.get());
    }

    @Test
    public void ignoreThen01() {
        final Source<Character> src = from("abcde");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,String> cde = a.ignoreThen(b).ignoreThen(string("cde"));
        final Result<Character,String> r = cde.apply(src);
        assertTrue(isSuccess(r));
        assertEquals("cde", r.get());
    }

    @Test
    public void ignoreThen02() {
        final Source<Character> src = from("abced");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,String> cde = a.ignoreThen(b).ignoreThen(string("cde"));
        final Result<Character,String> r = cde.apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void thenIgnore01() {
        final Source<Character> src = from("abcde");
        final Parser<Character,String> ab = string("ab").thenIgnore(string("cde"));
        final Result<Character,String> r = ab.apply(src);
        assertTrue(isSuccess(r));
        assertEquals("ab", r.get());
    }

    @Test
    public void thenIgnore02() {
        final Source<Character> src = from("bacde");
        final Parser<Character,String> ab = string("ab").thenIgnore(string("cde"));
        final Result<Character,String> r = ab.apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void thenIgnore03() {
        final Source<Character> src = from("abdee");
        final Parser<Character,String> ab = string("ab").thenIgnore(string("cde"));
        final Result<Character,String> r = ab.apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void sequence01() {
        final Source<Character> src = from("abcde");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,Seq<Character>> ab = Parser.sequence(cons(a, cons(b, empty())));
        final Result<Character,Seq<Character>> r = ab.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.from('a','b'), r.get());
    }

    @Test
    public void or01() {
        final Source<Character> src = from("abcde");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Result<Character,Character> r = a.or(b).apply(src);
        assertTrue(isSuccess(r));
        assertEquals('a', (char)r.get());
    }

    @Test
    public void or02() {
        final Source<Character> src = from("bacde");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Result<Character,Character> r = a.or(b).apply(src);
        assertTrue(isSuccess(r));
        assertEquals('b', (char)r.get());
    }

    @Test
    public void or03() {
        final Source<Character> src = from("cabde");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Result<Character,Character> r = a.or(b).apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void eof01() {
        final Source<Character> src = from("abc");
        final Parser<Character,Unit> abc = string("abc").ignoreThen(Parser.eof());
        final Result<Character,Unit> r = abc.apply(src);
        assertTrue(isSuccess(r));
    }

    @Test
    public void eof02() {
        final Source<Character> src = from("abcd");
        final Parser<Character,Unit> abc = string("abc").ignoreThen(Parser.eof());
        final Result<Character,Unit> r = abc.apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void atLeast0_01() {
        final Source<Character> src = from("bbb");
        final Parser<Character,Seq<Character>> a = Parser.atLeast0(character('a'));
        final Result<Character,Seq<Character>> r = a.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.empty(), r.get());
    }

    @Test
    public void atLeast0_02() {
        final Source<Character> src = from("abb");
        final Parser<Character,Seq<Character>> a = Parser.atLeast0(character('a'));
        final Result<Character,Seq<Character>> r = a.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.from('a'), r.get());
    }

    @Test
    public void atLeast0_03() {
        final Source<Character> src = from("aab");
        final Parser<Character,Seq<Character>> a = Parser.atLeast0(character('a'));
        final Result<Character,Seq<Character>> r = a.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.from('a', 'a'), r.get());
    }

    @Test
    public void atLeast1_01() {
        final Source<Character> src = from("bbb");
        final Parser<Character,Seq<Character>> a = Parser.atLeast1(character('a'));
        final Result<Character,Seq<Character>> r = a.apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void atLeast1_02() {
        final Source<Character> src = from("abb");
        final Parser<Character,Seq<Character>> a = Parser.atLeast1(character('a'));
        final Result<Character,Seq<Character>> r = a.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.from('a'), r.get());
    }

    @Test
    public void atLeast1_03() {
        final Source<Character> src = from("aab");
        final Parser<Character,Seq<Character>> a = Parser.atLeast1(character('a'));
        final Result<Character,Seq<Character>> r = a.apply(src);
        assertTrue(isSuccess(r));
        assertEquals(Seq.from('a', 'a'), r.get());
    }

    @Test
    public void oneOf01() {
        final Source<Character> src = from("a");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,Character> c = character('c');
        final Parser<Character,Character> d = character('d');
        final Parser<Character,Character> abcd = Parser.oneOf(a, b, c, d);
        final Result<Character,Character> r = abcd.apply(src);
        assertTrue(isSuccess(r));
        assertEquals('a', (char)r.get());
    }

    @Test
    public void oneOf02() {
        final Source<Character> src = from("b");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,Character> c = character('c');
        final Parser<Character,Character> d = character('d');
        final Parser<Character,Character> abcd = Parser.oneOf(a, b, c, d);
        final Result<Character,Character> r = abcd.apply(src);
        assertTrue(isSuccess(r));
        assertEquals('b', (char)r.get());
    }

    @Test
    public void oneOf03() {
        final Source<Character> src = from("d");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,Character> c = character('c');
        final Parser<Character,Character> d = character('d');
        final Parser<Character,Character> abcd = Parser.oneOf(a, b, c, d);
        final Result<Character,Character> r = abcd.apply(src);
        assertTrue(isSuccess(r));
        assertEquals('d', (char)r.get());
    }

    @Test
    public void oneOf04() {
        final Source<Character> src = from("f");
        final Parser<Character,Character> a = character('a');
        final Parser<Character,Character> b = character('b');
        final Parser<Character,Character> c = character('c');
        final Parser<Character,Character> d = character('d');
        final Parser<Character,Character> abcd = Parser.oneOf(a, b, c, d);
        final Result<Character,Character> r = abcd.apply(src);
        assertFalse(isSuccess(r));
    }

    @Test
    public void surroundedBy01() {
        final Source<Character> src = from("(abc)");
        final Parser<Character,String> abc = string("abc").surroundedBy(character('('), character(')'));
        final Result<Character,String> r = abc.apply(src);
        assertTrue(isSuccess(r));
        assertEquals("abc", r.get());
    }

    @Test
    public void separatedBy01() {
        final Source<Character> src = from("");
        final Parser<Character,String> empty = letter().separatedBy(character(',')).map(x -> Seq.toString(x));
        final Result<Character,String> r = empty.apply(src);
        assertTrue(isSuccess(r));
        assertEquals("", r.get());
    }

    @Test
    public void separatedBy02() {
        final Source<Character> src = from("a");
        final Parser<Character,String> a = letter().separatedBy(character(',')).map(x -> Seq.toString(x));
        final Result<Character,String> r = a.apply(src);
        assertTrue(isSuccess(r));
        assertEquals("a", r.get());
    }

    @Test
    public void separatedBy03() {
        final Source<Character> src = from("[a,b,c,d]");
        final Parser<Character,String> abcd = letter()
                .separatedBy(character(','))
                .surroundedBy(character('['), character(']'))
                .map(x -> Seq.toString(x));
        final Result<Character,String> r = abcd.apply(src);
        assertTrue(isSuccess(r));
        assertEquals("abcd", r.get());
    }

    @Test
    public void separatedBy04() {
        final Source<Character> src = from("[a,b,4,d]");
        final Parser<Character,String> abcd = letter()
                .separatedBy(character(','))
                .surroundedBy(character('['), character(']'))
                .map(x -> Seq.toString(x));
        final Result<Character,String> r = abcd.apply(src);
        assertFalse(isSuccess(r));
    }
}
