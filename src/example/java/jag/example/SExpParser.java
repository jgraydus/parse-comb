package jag.example;

import jag.parser.comb.*;

import static jag.parser.comb.Parser.*;
import static jag.parser.comb.Seq.cons;
import static jag.parser.comb.Unit.unit;

/**
 * A parser for a simple S-Expression format. <br>
 * <br>
 * example: <br>
 * <pre>
 * (def x (add 1 2 3))
 * </pre>
 * is parsed into the following data structure: <br>
 * <pre>
 * SList
 *   Sym "def"
 *   Sym "x"
 *   SList
 *     Sym "add"
 *     Int 1
 *     Int 2
 *     Int 3
 * </pre>
 * The supported type are lists, integers, strings, and symbols.
 */
public class SExpParser implements Parser<Character,SExp> {

    // a parser that recognizes 0 or more whitespace characters
    private final Parser<Character,Unit> optionalWhitespace =
            atLeast0(oneOf(character(' '), character('\n'), character('\t'))).map(x -> unit);

    // convert a parser p into a parser that recognizes and removes leading and trailing whitespace
    private <T> Parser<Character,T> trim(final Parser<Character,T> p) {
        return p.surroundedBy(optionalWhitespace, optionalWhitespace);
    }

    // a parser that recognizes a sequence of digits as an integer
    private final Parser<Character,SExp> integer = trim(atLeast1(digit()))
            .map(x -> Integer.parseInt(Seq.toString(x)))
            .map(SExp.Int::new);

    // a parser that recognizes any sequence of characters in quotes as a string
    private final Parser<Character,SExp> str = trim(atLeast0(satisfies((Character c) -> c != '\"'))
            .surroundedBy(character('\"'),character('\"')))
            .map(x -> new SExp.Str(Seq.toString(x)));

    // a parser that recognizes a letter followed by a sequence of letters and digits as a symbol
    private final Parser<Character,SExp> sym = trim(letter()
            .flatMap(d -> atLeast0(letter().or(digit())).map(ds -> cons(d,ds))))
            .map(x -> new SExp.Sym(Seq.toString(x)));

    // a parser that recognizes a quoted s-expression
    private final Parser<Character,SExp> quote = optionalWhitespace
            .ignoreThen(character('\''))
            .ignoreThen(this)
            .map(SExp.Quote::new);

    // a parser that recognizes a list of items
    private final Parser<Character,SExp> list = trim(separatedBy(optionalWhitespace))
            .surroundedBy(character('('),character(')'))
            .map(SExp.SList::new);

    @Override
    public Result<Character,SExp> apply(Source<Character> src) {
        return oneOf(quote, integer, str, sym, list).apply(src);
    }

    public static void main(String... args) {
        final String example = "(def stuff\n    '(\"hello\" 1 '2 x ''y '\"world\"))";
        System.out.println(new SExpParser().apply(Source.from(example)).get());
    }
}
