package jag.example;

import jag.parser.comb.Result;
import jag.parser.comb.Seq;
import jag.parser.comb.Source;
import org.junit.Test;

import static jag.example.SExp.*;
import static jag.parser.comb.Result.isSuccess;
import static jag.parser.comb.Seq.empty;
import static jag.parser.comb.Seq.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SExpParserTest {

    private final SExpParser p = new SExpParser();

    @Test
    public void test01() {
        final Source<Character> src = Source.from("()");
        final Result<Character,SExp> r = p.apply(src);
        assertTrue(isSuccess(r));
        final SExp actual = r.get();
        final SExp expected = new SList(empty());
        assertEquals(expected, actual);
    }

    @Test
    public void test02() {
        final Source<Character> src = Source.from("(42 foo \"b a r\")");
        final Result<Character,SExp> r = p.apply(src);
        assertTrue(isSuccess(r));
        final SExp actual = r.get();
        final SExp expected = new SList(from(new Int(42), new Sym("foo"), new Str("b a r")));
        assertEquals(expected, actual);
    }

    @Test
    public void test03() {
        final Source<Character> src = Source.from("( (    )(() ))");
        final Result<Character,SExp> r = p.apply(src);
        assertTrue(isSuccess(r));
        final SExp actual = r.get();
        final SExp expected = new SList(Seq.from(new SList(Seq.empty()), new SList(from(new SList(Seq.empty())))));
        assertEquals(expected, actual);
    }

    @Test
    public void test04() {
        final Source<Character> src = Source.from("(def x(add 1 2 3))");
        final Result<Character,SExp> r = p.apply(src);
        assertTrue(isSuccess(r));
        final SExp actual = r.get();
        final SExp expected = new SList(Seq.from(new Sym("def"), new Sym("x"), new SList(Seq.from(new Sym("add"),
                new Int(1), new Int(2), new Int(3)))));
        assertEquals(expected, actual);
    }

    @Test
    public void test05() {
        final Source<Character> src = Source.from("('() 'x '(a))");
        final Result<Character,SExp> r = p.apply(src);
        assertTrue(isSuccess(r));
        final SExp actual = r.get();
        final SExp expected = new SList(Seq.from(new Quote(new SList(Seq.empty())), new Quote(new Sym("x")),
                new Quote(new SList(Seq.from(new Sym("a"))))));
        assertEquals(expected, actual);
    }
}
