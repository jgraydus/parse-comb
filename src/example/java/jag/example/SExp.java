package jag.example;

import jag.parser.comb.Seq;

/** AST (abstract syntax tree) for s-expressions */
public interface SExp {

    /** Int represents an integer literal */
    final class Int implements SExp {
        private final int value;
        public Int(final int value) { this.value = value; }
        @Override public String toString() { return Integer.toString(value); }
        @Override public boolean equals(final Object o) { return o instanceof Int && value == ((Int)o).value; }
    }

    /** Str represents a string literal */
    final class Str implements SExp {
        private final String value;
        public Str(final String value) { this.value = value; }
        @Override public String toString() { return "\"" + value + "\""; }
        @Override public boolean equals(final Object o) { return o instanceof Str && value.equals(((Str)o).value); }
    }

    /** Sym represents a symbol literal */
    final class Sym implements SExp {
        private final String value;
        public Sym(final String value) { this.value = value; }
        @Override public String toString() { return value; }
        @Override public boolean equals(final Object o) { return o instanceof Sym && value.equals(((Sym)o).value); }
    }

    /** Quote represents a quoted s-expression */
    final class Quote implements SExp {
        private final SExp exp;
        public Quote(final SExp exp) { this.exp = exp; }
        @Override public String toString() { return "[quote " + exp.toString() + "]"; }
        @Override public boolean equals(final Object o) { return o instanceof Quote && exp.equals(((Quote)o).exp); }
    }

    /** SList represents a list of s-expressions */
    final class SList implements SExp {
        private final Seq<SExp> sexp;
        public SList(final Seq<SExp> sexp) { this.sexp = sexp; }
        @Override public String toString() { return sexp.toString(); }
        @Override public boolean equals(final Object o) { return o instanceof SList && sexp.equals(((SList)o).sexp); }
    }
}
