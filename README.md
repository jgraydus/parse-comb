# java-parser-combinator

## Description

This is a parser combinator library I wrote for fun.

Goals:
  - simple
  - purely functional
  - self-contained / no dependencies

Not goals:
  - good error messages

Overview:

All parsers and combinators live in the `Parser` interface. A parser reads a stream of tokens 
represented by a `Source` object. The `Source` interface provides functions for converting a 
Java `String` or `List` into a `Source`. Wherever lists are needed, the library uses `Seq`, an
immutable linked list implementation. `Seq` provides functions for converting to and from Java
`List`s and `String`s.

## Example

The following code can be found in `src/example/java`. In this example, we create a parser for a simple
s-expression format. It supports parsing integers, strings, symbols, and arbitrarily nested lists. It
can also handle quoted items such as `'x` and `'(1 2 3)`.

We start by creating a class which implements `Parser<Character,SExp>`. 
This means that the parser will read tokens which are `Character`s and, if the parse succeeds,
will produce an `SExp` object.

    public class SExpParser implements Parser<Character,SExp> {

We will find it convenient to be able to trim whitespace, so we write a parser that recognizes
zero or more spaces, tabs, or newlines. This parser returns a `Unit` since we don't really care what
result it produces.

        private final Parser<Character,Unit> optionalWhitespace =
                atLeast0(oneOf(character(' '), character('\n'), character('\t')))
                .map(x -> unit);

Every parser comes with a method called `surroundedBy` which we use to implement a utility function
`trim`. When given a parser, `trim` returns a parser that does the same thing as the original except 
that it ignores leading and trailing whitespace.

        private final <T> Parser<Character,T> trim(final Parser<Character,T> p) {
            return p.surroundedBy(optionalWhitespace, optionalWhitespace);
        }

Now we implement individual parsers for each of the types of s-expression objects. First, the integer 
type which is probably the easiest. An integer is any consecutive string of digits. Note that we
convert the collected digits to a Java `String` and ask the `Integer#parseInt` function convert 
it to a Java `Integer`.

        private final Parser<Character,SExp> integer = trim(atLeast1(digit()))
                .map(x -> Integer.parseInt(Seq.toString(x)))
                .map(SExp.Int::new);

A string is any sequence of characters that does not contain a double quote character and is 
surrounded by double quote characters.

        private final Parser<Character,SExp> str = trim(atLeast0(Parser.<Character>satisfies(c -> c != '\"'))
                .surroundedBy(character('\"'),character('\"')))
                .map(x -> new SExp.Str(Seq.toString(x)))
    
Symbols are required to start with a letter, but the remaining characters can be either letters or 
digits.
    
        private final Parser<Character,SExp> sym = trim(letter()
                .flatMap(d -> atLeast0(letter().or(digit())).map(ds -> cons(d,ds))))
                .map(x -> new SExp.Sym(Seq.toString(x)))

This is where it starts to get interesting. Any s-expression can be quoted by prefixing it with 
a `'` character. So to parse a quoted expression, we simply need to recognize the quote and then 
call the full parser (`this`) recursively to get the expression.

        private final Parser<Character,SExp> quote = optionalWhitespace
                .ignoreThen(character('\''))
                .ignoreThen(this)
                .map(SExp.Quote::new);

Recognizing a list is also a recursive actively. A list is any sequence of s-expressions
enclosed in parentheses. The recursion isn't obvious. It happens with the call to separatedBy 
which is a method of `this`.

        private final Parser<Character,SExp> list = trim(separatedBy(optionalWhitespace))
                .surroundedBy(character('('),character(')'))
                .map(SExp.SList::new);

Finally, we put it all together. An s-expression is a quoted s-expression, an integer, 
a string, a symbol, or a list of s-expressions.

        @Override
        public Result<Character,SExp> apply(Source<Character> src) {
            return oneOf(quote, integer, str, sym, list).apply(src);
        }
    }

#### Demonstration of the s-expression parser

Parsing the string

    (def stuff '("hello" 1 '2 x ''y '"world"))
    
results in an SExp object whose toString method produces

    [def, stuff, [quote ["hello", 1, [quote 2], x, [quote [quote y]], [quote "world"]]]]